﻿using Domain.Contracts.Repositories.AddSale;
using Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repository.Repositories.AddSale
{
    public class AddSaleRepository : IAddSaleRepository
    {
        private readonly IList<Sale> _sales = new List<Sale>();
        public void AddSale(Sale sale)
        {
            _sales.Add(sale);
        }

        public IList<Sale> GetSale(int id)
        {

            var listSale = _sales.Where(x => x.Id == id).ToList();

            return listSale;
        }

        public void UpdateSale(int id, SaleStatus saleStatus)
        {
            _sales.Single(x => x.Id == id).Status = saleStatus;

        }




    }
}
