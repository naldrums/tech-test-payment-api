﻿using Domain.Contracts.Repositories.AddSale;
using Domain.Contracts.UseCases.AddSale;
using Domain.Entities;

namespace Application.UseCases.AddSale
{
    public class AddSaleUseCase : IAddSaleUseCase
    {
        private readonly IAddSaleRepository _addSaleRepository;

        public AddSaleUseCase(IAddSaleRepository addSaleRepository)
        {
            _addSaleRepository = addSaleRepository;
        }

        public void AddSale(Sale sale)
        {
            _addSaleRepository.AddSale(sale);
        }

        public IList<Sale> GetSale(int id)
        {
            return _addSaleRepository.GetSale(id);
        }

        public void UpdateSale(int id, SaleStatus salestatus)
        {

            _addSaleRepository.UpdateSale(id, salestatus);
        }
    }
}
