﻿namespace TesteUltimoDesafioDIO.Models
{
    public class Salesman
    {

        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
    }
}
