﻿using Domain.Entities;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace TesteUltimoDesafioDIO.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum SaleStatus : int
    {
        AwaitingPayment = 0,
        SentToCarrier = 1,
        Delivered = 2,
        Canceled = 3,
        PaymentAccept = 4

    }
}
