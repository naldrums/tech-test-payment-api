﻿using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.Reflection.Metadata.Ecma335;

namespace TesteUltimoDesafioDIO.Models.AddSale
{

    public class Sale
    {
        public int Id { get; set; }
        public Salesman Salesmen { get; set; }
        public DateTime DateOfSale { get; set; }
        public SaleStatus Status { get; set; }
        public List<Product> ListProducts { get; set; }

        public class Product
        {
            public int IdProduct { get; set; }
            public string NameProduct { get; set; }

        }




    }




}
