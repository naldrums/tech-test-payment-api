﻿using Domain.Contracts.UseCases.AddSale;
using Domain.Entities;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TesteUltimoDesafioDIO.Models;

namespace TesteUltimoDesafioDIO.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddSaleController : ControllerBase
    {
        private readonly IAddSaleUseCase _addSaleUseCase;

        public AddSaleController(IAddSaleUseCase addSaleUseCase)
        {
            _addSaleUseCase = addSaleUseCase;
        }

        [HttpPost]
        public IActionResult AddSale(Models.AddSale.Sale sale)
        {

            var salesman = new Domain.Entities.Salesman
            {
                Id = sale.Salesmen.Id,
                Cpf = sale.Salesmen.Cpf,
                Name = sale.Salesmen.Name,
                Email = sale.Salesmen.Email,
                Telephone = sale.Salesmen.Telephone,
            };

            var product = new List<Product>();

            foreach (var item in sale.ListProducts)
            {

                product.Add(new Product()
                {

                    IdProduct = item.IdProduct,
                    NameProduct = item.NameProduct


                });
            }

            var obj = new Sale(sale.Id, salesman, product, DateTime.Now, (Domain.Entities.SaleStatus)sale.Status);


            _addSaleUseCase.AddSale(obj);

            return Created("Sucesso", obj);
        }


        [HttpPut]
        [Route("UpdateSale")]
        public IActionResult UpdateSale(int id, Models.SaleStatus saleStatus)
        {
            var obj = new Sale();
            var saleList = _addSaleUseCase.GetSale(id);

            if (saleList.Count == 0)
                return NotFound();


            if (obj.StatusChange(saleList, (Domain.Entities.SaleStatus)saleStatus))
            {
                _addSaleUseCase.UpdateSale(id, (Domain.Entities.SaleStatus)saleStatus);
            }
            else
            {
                return StatusCode(406, "Operação não permitida");
            }


            return Ok();

        }

        [HttpGet]
        [Route("GetSaleById")]
        public IActionResult GetSaleById(int id)
        {
            var saleList = _addSaleUseCase.GetSale(id);

            if (saleList.Count == 0)
                return NotFound();

            return Ok(saleList);
        }


    }
}
