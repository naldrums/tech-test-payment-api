﻿using Domain.Entities;

namespace Domain.Contracts.UseCases.AddSale
{
    public interface IAddSaleUseCase
    {
        void AddSale(Sale sale);

        void UpdateSale(int id, SaleStatus saleStatus);

        public IList<Sale> GetSale(int id);


    }
}
