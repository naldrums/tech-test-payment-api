﻿using Domain.Entities;

namespace Domain.Contracts.Repositories.AddSale
{
    public interface IAddSaleRepository
    {
        void AddSale(Sale sale);

        void UpdateSale(int id, SaleStatus saleStatus);

        public IList<Sale> GetSale(int id);
    }
}
