﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Sale
    {
        public Sale()
        {
        }

        public Sale(int id, Salesman salesmen, List<Product> listProduct, DateTime dateOfSale, SaleStatus status)
        {
            Id = id;
            Salesmen = salesmen;
            ListProducts = listProduct;
            DateOfSale = dateOfSale;
            Status = status;
        }

        public int Id { get; set; }
        public Salesman Salesmen { get; set; }
        public DateTime DateOfSale { get; set; }
        public SaleStatus Status { get; set; }
        public List<Product> ListProducts { get; set; }



        public bool StatusChange(IList<Sale> saleList, SaleStatus saleStatus)
        {
            if (saleList[0].Status == SaleStatus.AwaitingPayment)
            {
                if (saleStatus == SaleStatus.PaymentAccept || saleStatus == SaleStatus.Canceled)
                {
                    return true;
                }
            }
            else if (saleList[0].Status == SaleStatus.PaymentAccept)
            {
                if (saleStatus == SaleStatus.SentToCarrier || saleStatus == SaleStatus.Canceled)
                {
                    return true;
                }
            }
            else if (saleList[0].Status == SaleStatus.SentToCarrier)
            {
                if (saleStatus == SaleStatus.Delivered)
                {
                    return true;
                }
            }

            return false;
        }



    }

    public class Product
    {
        public int IdProduct { get; set; }
        public string NameProduct { get; set; }

    }


}
