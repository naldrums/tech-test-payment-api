﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public enum SaleStatus : int
    {
        AwaitingPayment = 0,
        SentToCarrier = 1,
        Delivered = 2,
        Canceled = 3,
        PaymentAccept = 4

    }
}
